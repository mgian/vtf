<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="it_IT">
<context>
    <name>Vtf</name>
    <message>
        <location filename="vtf.ui" line="14"/>
        <source>Velocità Taglio Frese Ver. 0.1</source>
        <translation>Cutter cutting speed</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="20"/>
        <source>Parametri utensile</source>
        <translation>Tools parameters</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="26"/>
        <source>Numero Denti</source>
        <translation>Teeth number</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="43"/>
        <source>Velocità taglio</source>
        <translation>Cutting speed</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="66"/>
        <source>m/min</source>
        <translation>m/min</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="172"/>
        <source>Avanzamento</source>
        <translation>Feed rate</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="96"/>
        <source>mm/dente</source>
        <translation>mm/tooth</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="103"/>
        <source>Diametro</source>
        <translation>Diameter</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="110"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="152"/>
        <source>Parametri macchina</source>
        <translation>Machine parameters</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="158"/>
        <source>Numero di giri</source>
        <translation>Rpm</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="165"/>
        <source>giri/min</source>
        <translation>rev/min</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="179"/>
        <source>mm/min</source>
        <translation>mm/min</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="217"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="261"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="vtf.ui" line="264"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
</context>
</TS>
