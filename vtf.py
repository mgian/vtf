#!/usr/bin/env python

# system include
import sys
import os
import re

from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QTranslator
from PySide2.QtWidgets import QLabel
from PySide2.QtWidgets import QDialog

# Specific include
from vtf_gui import Ui_Vtf


class VtfWindow(Ui_Vtf):
    def __init__(self):
        self.giri = 0
        self.denti = 1
        self.ava_dente = 0.0
        self.vel_ava = 0.0
        self.diametro = 0.0
        self.vel_taglio = 0.0

    def FinalizeGui(self):
        ui.SP_Denti.valueChanged.connect(self.DentiMod)
        ui.LE_Diametro.textChanged.connect(self.DiamMod)
        ui.LE_VelTaglio.textChanged.connect(self.VelTaglioMod)
        ui.LE_AvaDente.textChanged.connect(self.AvaDentMod)

    def UpdateVel(self):
        # Calcolo il numero di giri
        # Controllo che tutti i valori necessari siano diversi da zero
        if self.vel_taglio == 0 or self.diametro == 0:
            giri_min = 0
        else:
            giri_min = int((self.vel_taglio * 1000) / self.diametro / 3.14)
        ui.LE_Giri.setText(str(giri_min))
        self.giri = giri_min
        self.UpdateAv()

    def UpdateAv(self):
        if self.giri == 0 or self.ava_dente == 0:
            ava_min = 0
        else:
            ava_min = self.giri * self.denti * self.ava_dente
        ui.LE_Avanzamento.setText(str(ava_min))

    def DentiMod(self, i):
        if i == 0:
            return
        self.denti = i
        self.UpdateVel()

    def DiamMod(self, text):
        try:
            self.diametro = float(text)
            self.UpdateVel()
        except:
            ui.LE_Diametro.setText(text[:-1])

    def VelTaglioMod(self, text):
        try:
            self.vel_taglio = float(text)
            self.UpdateVel()
        except:
            ui.LE_VelTaglio.setText(text[:-1])

    def AvaDentMod(self, text):
        try:
            self.ava_dente = float(text)
            self.UpdateVel()
        except:
            ui.LE_AvaDente.setText(text[:-1])

translator = QTranslator()
translator.load('i18n/en_GB')
app = QApplication(sys.argv)
app.installTranslator(translator)
window = QDialog()
ui = VtfWindow()
ui.setupUi(window)
ui.FinalizeGui()
window.show()
sys.exit(app.exec_())
