# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'vtf.ui'
##
## Created by: Qt User Interface Compiler version 5.14.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *

class Ui_Vtf(object):
    def setupUi(self, Vtf):
        if Vtf.objectName():
            Vtf.setObjectName(u"Vtf")
        Vtf.resize(276, 308)
        self.vboxLayout = QVBoxLayout(Vtf)
        self.vboxLayout.setObjectName(u"vboxLayout")
        self.GB_ToolParams = QGroupBox(Vtf)
        self.GB_ToolParams.setObjectName(u"GB_ToolParams")
        self.gridLayout = QGridLayout(self.GB_ToolParams)
        self.gridLayout.setObjectName(u"gridLayout")
        self.L_TeethNumber = QLabel(self.GB_ToolParams)
        self.L_TeethNumber.setObjectName(u"L_TeethNumber")

        self.gridLayout.addWidget(self.L_TeethNumber, 0, 0, 1, 1)

        self.SP_Denti = QSpinBox(self.GB_ToolParams)
        self.SP_Denti.setObjectName(u"SP_Denti")
        self.SP_Denti.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.SP_Denti.setMinimum(1)

        self.gridLayout.addWidget(self.SP_Denti, 0, 1, 1, 1)

        self.L_CuttingSpeed = QLabel(self.GB_ToolParams)
        self.L_CuttingSpeed.setObjectName(u"L_CuttingSpeed")

        self.gridLayout.addWidget(self.L_CuttingSpeed, 2, 0, 1, 1)

        self.LE_VelTaglio = QLineEdit(self.GB_ToolParams)
        self.LE_VelTaglio.setObjectName(u"LE_VelTaglio")
        self.LE_VelTaglio.setMaxLength(8)
        self.LE_VelTaglio.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.LE_VelTaglio, 2, 1, 1, 1)

        self.label_3 = QLabel(self.GB_ToolParams)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 2, 2, 1, 1)

        self.L_ToolFeed = QLabel(self.GB_ToolParams)
        self.L_ToolFeed.setObjectName(u"L_ToolFeed")

        self.gridLayout.addWidget(self.L_ToolFeed, 3, 0, 1, 1)

        self.LE_AvaDente = QLineEdit(self.GB_ToolParams)
        self.LE_AvaDente.setObjectName(u"LE_AvaDente")
        self.LE_AvaDente.setMaxLength(7)
        self.LE_AvaDente.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.LE_AvaDente, 3, 1, 1, 1)

        self.label_5 = QLabel(self.GB_ToolParams)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout.addWidget(self.label_5, 3, 2, 1, 1)

        self.L_Diameter = QLabel(self.GB_ToolParams)
        self.L_Diameter.setObjectName(u"L_Diameter")

        self.gridLayout.addWidget(self.L_Diameter, 1, 0, 1, 1)

        self.label_7 = QLabel(self.GB_ToolParams)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout.addWidget(self.label_7, 1, 2, 1, 1)

        self.spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.spacerItem, 0, 2, 1, 1)

        self.LE_Diametro = QLineEdit(self.GB_ToolParams)
        self.LE_Diametro.setObjectName(u"LE_Diametro")
        self.LE_Diametro.setMaxLength(7)
        self.LE_Diametro.setCursorPosition(0)
        self.LE_Diametro.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.LE_Diametro, 1, 1, 1, 1)


        self.vboxLayout.addWidget(self.GB_ToolParams)

        self.GB_MachineParam = QGroupBox(Vtf)
        self.GB_MachineParam.setObjectName(u"GB_MachineParam")
        self.gridLayout1 = QGridLayout(self.GB_MachineParam)
        self.gridLayout1.setObjectName(u"gridLayout1")
        self.L_Rpm = QLabel(self.GB_MachineParam)
        self.L_Rpm.setObjectName(u"L_Rpm")

        self.gridLayout1.addWidget(self.L_Rpm, 0, 0, 1, 1)

        self.label_12 = QLabel(self.GB_MachineParam)
        self.label_12.setObjectName(u"label_12")

        self.gridLayout1.addWidget(self.label_12, 0, 2, 1, 1)

        self.L_MachineFeed = QLabel(self.GB_MachineParam)
        self.L_MachineFeed.setObjectName(u"L_MachineFeed")

        self.gridLayout1.addWidget(self.L_MachineFeed, 1, 0, 1, 1)

        self.label_13 = QLabel(self.GB_MachineParam)
        self.label_13.setObjectName(u"label_13")

        self.gridLayout1.addWidget(self.label_13, 1, 2, 1, 1)

        self.LE_Giri = QLineEdit(self.GB_MachineParam)
        self.LE_Giri.setObjectName(u"LE_Giri")
        self.LE_Giri.setAutoFillBackground(False)
        self.LE_Giri.setMaxLength(32767)
        self.LE_Giri.setEchoMode(QLineEdit.Normal)
        self.LE_Giri.setCursorPosition(1)
        self.LE_Giri.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.LE_Giri.setReadOnly(False)

        self.gridLayout1.addWidget(self.LE_Giri, 0, 1, 1, 1)

        self.LE_Avanzamento = QLineEdit(self.GB_MachineParam)
        self.LE_Avanzamento.setObjectName(u"LE_Avanzamento")
        self.LE_Avanzamento.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.LE_Avanzamento.setReadOnly(False)

        self.gridLayout1.addWidget(self.LE_Avanzamento, 1, 1, 1, 1)


        self.vboxLayout.addWidget(self.GB_MachineParam)

        self.spacerItem1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.vboxLayout.addItem(self.spacerItem1)

        self.hboxLayout = QHBoxLayout()
        self.hboxLayout.setObjectName(u"hboxLayout")
        self.spacerItem2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hboxLayout.addItem(self.spacerItem2)

        self.B_Quit = QPushButton(Vtf)
        self.B_Quit.setObjectName(u"B_Quit")
        self.B_Quit.setAutoDefault(False)
        self.B_Quit.setFlat(False)

        self.hboxLayout.addWidget(self.B_Quit)


        self.vboxLayout.addLayout(self.hboxLayout)

        QWidget.setTabOrder(self.SP_Denti, self.LE_Diametro)
        QWidget.setTabOrder(self.LE_Diametro, self.LE_VelTaglio)
        QWidget.setTabOrder(self.LE_VelTaglio, self.LE_AvaDente)
        QWidget.setTabOrder(self.LE_AvaDente, self.LE_Giri)
        QWidget.setTabOrder(self.LE_Giri, self.LE_Avanzamento)
        QWidget.setTabOrder(self.LE_Avanzamento, self.B_Quit)

        self.retranslateUi(Vtf)
        self.B_Quit.clicked.connect(Vtf.accept)

        QMetaObject.connectSlotsByName(Vtf)
    # setupUi

    def retranslateUi(self, Vtf):
        Vtf.setWindowTitle(QCoreApplication.translate("Vtf", u"Velocit\u00e0 Taglio Frese Ver. 0.1", None))
        self.GB_ToolParams.setTitle(QCoreApplication.translate("Vtf", u"Parametri utensile", None))
        self.L_TeethNumber.setText(QCoreApplication.translate("Vtf", u"Numero Denti", None))
        self.L_CuttingSpeed.setText(QCoreApplication.translate("Vtf", u"Velocit\u00e0 taglio", None))
        self.LE_VelTaglio.setInputMask("")
        self.LE_VelTaglio.setText("")
        self.label_3.setText(QCoreApplication.translate("Vtf", u"m/min", None))
        self.L_ToolFeed.setText(QCoreApplication.translate("Vtf", u"Avanzamento", None))
        self.LE_AvaDente.setInputMask("")
        self.LE_AvaDente.setText("")
        self.label_5.setText(QCoreApplication.translate("Vtf", u"mm/dente", None))
        self.L_Diameter.setText(QCoreApplication.translate("Vtf", u"Diametro", None))
        self.label_7.setText(QCoreApplication.translate("Vtf", u"mm", None))
        self.LE_Diametro.setInputMask("")
        self.LE_Diametro.setText("")
        self.GB_MachineParam.setTitle(QCoreApplication.translate("Vtf", u"Parametri macchina", None))
        self.L_Rpm.setText(QCoreApplication.translate("Vtf", u"Numero di giri", None))
        self.label_12.setText(QCoreApplication.translate("Vtf", u"giri/min", None))
        self.L_MachineFeed.setText(QCoreApplication.translate("Vtf", u"Avanzamento", None))
        self.label_13.setText(QCoreApplication.translate("Vtf", u"mm/min", None))
        self.LE_Giri.setInputMask("")
        self.LE_Giri.setText(QCoreApplication.translate("Vtf", u"0", None))
        self.LE_Avanzamento.setInputMask("")
        self.LE_Avanzamento.setText(QCoreApplication.translate("Vtf", u"0", None))
        self.B_Quit.setText(QCoreApplication.translate("Vtf", u"&Quit", None))
#if QT_CONFIG(shortcut)
        self.B_Quit.setShortcut(QCoreApplication.translate("Vtf", u"Ctrl+Q", None))
#endif // QT_CONFIG(shortcut)
    # retranslateUi

